import React, {Component} from 'react';

class PersonAdd extends Component {

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit (e){
        this.props.addPerson(this.name.value, this.surname.value);
        e.preventDefault();
    };


    render() {
        return (
            <div className="container">
                <div className="row" >
                    <form onSubmit={this.handleSubmit}>
                    <div className="col-12">
                        <input type="text" className="form-control" placeholder="Имя" ref={(input) => this.name = input} required/>
                    </div>
                    <div className="col-12">
                        <input type="text" className="form-control" placeholder="Фамилия" ref={(input) => this.surname = input} required/>
                    </div>
                    <div className="col-12">
                        <button type="submit" className="btn btn-primary mt-2">Сохранить</button>
                    </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default PersonAdd;
